# README

**Library Overview** 

*Main Public Classes*

- **setCompanyHeader**
    + This handles all issuing company data such as address, contact info, name, and logo.
        + All Arguments passed as associative array.  
        + Required Arguments: CompanyName,CompanyAddress,CompanyState,CompanyCity,CompanyZip,docType
        + Optional Arguments: CompanyPhone, CompanyFax
**Example**

```
include_once('PDF_Document.php');
$pdf = new PDF_Document;

$pdf->setCompanyHeader(
    [
        "CompanyName" => "Nathan's Company",
        "CompanyAddress" => "123456 Main Street",
        "CompanyState" => 'OH',
        "CompanyCity" => 'Akron',
        "CompanyZip" => "11234",
        "CompanyPhone" => "330-945-1243",
        "CompanyFax" => "123-123-1234",
        "docType" => "PO",
        "logo" => "logo.png"
]
);
```

- **setDocumentData** 
    + This handles all issuing data such as document number, bill to, ship to, document date and document type. 
    + You are limited to 10 custom fields in addition to the required arguments.  
        * This is two rows of 5 items each. The remaining fields after the mandatory fields are sent will be written to the page in order they are placed in the array. 
        * The Key of the custom fields will be used as the header for the custom field and be displayed above the value. 
    + The mandatory arguments are as follows 
        *  billTo
            -  Passed as array if no bill to please send `"billTo" => []`
        *  shipTo
            -  Passed as array if no ship to please send `"shipTo" => []`
        *  tax
            -  if no tax please send `"tax" => 0.00"`
        *  subtotal
            - if no subtotal please send `"subtotal" => "0.00"`
        *  shipping
            - if no shipping please send `"shipping" => "0.00"`
        *  total 
            - if no total please send `"total" => "0.00"`
        *  footerText
            - if no footerText please send `"footerText" =>""`
   
            
**Example** 
```
$pdf->setDocumentData(
    [
        "billTo"=>["Nathan's Company","123456 Main Street","Akron OH, 55432","United States"],
        "shipTo"=> ["Nathan's Company","123456 Main Street","Akron OH, 55432","United States"],
        "tax"=>"20.45",
        "subtotal"=>"10",
        "shipping"=>"22.36",
        "total"=>"100.00", 
        "footerText" => "Thanks for being a customer at Nathan's Company",
        "Invoice Number"=>"112ANF", 
        "Terms" => '30 Days',
        "PO Number" => '11243'
    ]
);
```

- **setLineItemWidth**
    + This function accepts an array of numbers to set the widths of the line item columns. 
    + If total of all columns is greater than 190 an error will be thrown. 
    + `$pdf->setLineItemWidth(array(38,38,38,38,38));`
- **setLineItemHeaders**
    + This function creates the labels for the line item data an will be written to the page in the order they appear in the array 
    + `$pdf->setLineItemHeaders(array('ItemID','Qty','PriceEa','Desc','Line Total'));`

- **setLineItemsData**
    + This accepts all line items related to the document 
    + Send this function a multidimensional array with the inner array containing the arguments in the same order as sent in setLineItemHeaders as they will be written to the table in that order.
    + These inner arrays must also include the same number of arguments sent in setlineItemWidth or an error will be thrown. 

```
$pdf->setLineItemsData(
    array(
        array("ABC123","10","1.88","IT does a thing", "18.80"),
        array("ABC124","10","1.88","IT does a thing", "18.80"),
        array("ABC125","10","1.88","IT does a thing", "18.80"),
        array("ABC126","10","1.88","IT does a thing", "18.80"),
        array("ABC127","10","1.88","IT does a thing", "18.80"),
        array("ABC128","10","1.88","IT does a thing", "18.80"),
        array("ABC128","10","1.88","IT does a thing", "18.80"),
        array("ABC128","10","1.88","IT does a thing", "18.80"),
        array("ABC128","10","1.88","IT does a thing", "18.80"),
        array("ABC128","10","1.88","IT does a thing", "18.80"),
        array("ABC128","10","1.88","IT does a thing", "18.80"),
        array("ABC129","10","1.88","IT does a thing", "18.80")
    )
);
```

- **CreateDocument** 
    + This will take all data set by the main three set functions and generate a document. 
    + This function requires no arguments 
    + `$pdf->createDocument();`
- **Output** 
    + This is a default function from the main FPDF library. 
    + More information can be found at [this link](http://www.fpdf.org/en/doc/output.htm)
