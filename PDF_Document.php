
<?php
require('library/PDF_FancyRow.php');
class PDF_Document extends PDF_FancyRow{

//private 

private $companyInfo = array();
private $lineItems = array();
private $documentData = array();
private $lineItemWidth = array();
private $lineItemHeaders = array();


private function CreateCompanyHeader(){
	//Create Company Header. Where all the nice info about the company goes. 
	$this->setXY(10,10);
	$this->SetFont('Arial','B',14);
	//Create Company header 
	$this->Cell(0,10,$this->companyInfo["CompanyName"],0,0,'L');
	$this->ln();
	$this->SetFont('Arial','',10);
	$this->Cell(0,5,$this->companyInfo["CompanyAddress"],0,0,'L');
	$this->ln();
	$this->Cell(0,5,$this->companyInfo["CompanyCity"].', '. $this->companyInfo["CompanyState"].' '. $this->companyInfo["CompanyZip"],0,0,'L');
	$this->ln();
	if(isset($this->companyInfo["CompanyPhone"])){
		$this->Cell(0,5,'Phone: '.$this->companyInfo["CompanyPhone"] ,0,0,'L');
		$this->ln(); 
	}
	if(isset($this->companyInfo["CompanyFax"])){
		$this->Cell(0,5,'Fax: '.$this->companyInfo["CompanyFax"] ,0,0,'L');
		$this->ln();
	}
	$this->ln();
	$this->SetFont('Arial','B', 20);
	//Add document Type 
	$this->Cell(0,10,$this->companyInfo['docType'] ,0,0,'C');
	if(isset($this->companyInfo["logo"])){
		$this->Image($this->companyInfo['logo'],3*((($this->GetPageWidth())-20)/4),10,60,'','','');
		$this->ln();
	}

}//End Create Company

private function CreateBillTo(){
	//creates Bill to Section
	$BoxWidth = (($this->GetPageWidth())-20)/2;
	$BoxStartX = 10;
	$BoxYStart = 60;
	$this->setXY($BoxStartX,$BoxYStart);
	$this->SetFont('Arial','B',12);
	$this->Cell($BoxWidth,10,'Bill to',1,0,'C');
	$this->setXY($BoxStartX,$BoxYStart);
	$this->Cell($BoxWidth,50,'',1,0,'L');
	$this->setXY($BoxStartX,$BoxYStart+13);
	$this->SetFont('Arial','',12);
	for ($i=0; $i < count($this->documentData['billTo']); $i++) { 
		$this->Cell(0,5,$this->documentData['billTo'][$i],2,0,'L');
		$this->ln();
		$this->setX($BoxStartX);
	}
	$this->ln();
}
private function CreateShipTo(){
	//Creates Ship to Section
	$BoxWidth = (($this->GetPageWidth())-20)/2;
	$BoxStartX = 105;
	$BoxYStart = 60;
	$this->setXY($BoxStartX,$BoxYStart);
	$this->SetFont('Arial','B',12);
	$this->Cell($BoxWidth,10,'Ship to',1,0,'C');
	$this->setXY($BoxStartX,$BoxYStart);
	$this->Cell($BoxWidth,50,'',1,0,'L');
	$this->setXY($BoxStartX,$BoxYStart+13);
	$this->SetFont('Arial','',12);
	$this->setX($BoxStartX);
	for ($i=0; $i < count($this->documentData['shipTo']); $i++) { 
		$this->Cell(0,5,$this->documentData['shipTo'][$i],2,0,'L');
		$this->ln();
		$this->setX($BoxStartX);
	}
	$this->ln();
}

private function CreateDetailsRow(){//This needs works... 
	$BoxWidth = (($this->GetPageWidth())-20)/2;
	$BoxStartX = 10;
	$BoxYStart = 110;
	$this->SetFont('Arial','B',12);
	$this->setXY($BoxStartX,$BoxYStart);
	$widths = array(38,38,38,38,38);
	$border = array('LBTR', 'LBTR', 'LBTR', 'LBTR', 'LBTR');
	$align = array('C', 'C', 'C', 'C', 'C');
	$styleHeader = array('B', 'B', 'B', 'B', 'B');
	$styleStandard = array('', '', '', '', '');
	$this->SetWidths($widths);
	$headerCaption = array();
	$valueCaption = array();
	$count =0;
	foreach ($this->documentData as $key => $value){
		if ($key !== "billTo" && $key !== "shipTo" && $key !== "total" && $key !== "shipping" && $key !== "tax" && $key !== "subtotal" && $key !== "footerText"){
			
			array_push($headerCaption, $key);
			array_push($valueCaption, $value);

			if(count($headerCaption)==5){
				$this->FancyRow($headerCaption, $border , $align, $styleHeader);
				$this->FancyRow($valueCaption, $border , $align, $styleStandard);
				$headerCaption = array();
				$valueCaption = array();
			}
			$count++;
			if($count > 10){
				throw new Exception("You have submitted to many custom data fields. Please limit the number of custom data fields to 10", 1);
			}
		}
	}
	if (count($headerCaption > 0)) {
		$this->FancyRow($headerCaption, $border , $align, $styleHeader);
		$this->FancyRow($valueCaption, $border , $align, $styleStandard);	
	}
}

private function CreateLineItems(){
	$BoxStartX = 10;
	$BoxYStart = 140;
	$this->setXY($BoxStartX,$BoxYStart);
	$this->createLineItemHeader();
	$this->SetFont('Arial','',12);
	$border = array('LBTR', 'LBTR', 'LBTR', 'LBTR', 'LBTR');
	$align = array('C', 'C', 'C', '', 'C');
	$style = array('', '', '', '', '');
	foreach ($this->lineItems as $row) {
		$this->FancyRow($row, $border , $align, $style);	
		if($this->getY() >= 200){
			$this->AddFooter();
			$this->addPage();
			$this->CreateCompanyHeader();
			$this->CreateBillTo();
			$this->CreateShipTo();
			$this->CreateDetailsRow();
			$this->createLineItemHeader();
		}
	}
	if($this->getY() < 200){
		$this->AddFooter();
	}

}
private function createLineItemHeader(){
	$BoxStartX = 10;
	$BoxYStart = 140;
	$this->setXY($BoxStartX,$BoxYStart);
	$this->SetFont('Arial','B',12);
	$widths = array(30,20,30,70,40);
	$border = array('LBTR', 'LBTR', 'LBTR', 'LBTR', 'LBTR');
	$caption = $this->lineItemHeaders;
	$align = array('C', 'C', 'C', 'C', 'C');
	$style = array('B', 'B', 'B', 'B', 'B');
	$this->SetWidths($this->lineItemWidth);
	$this->FancyRow($caption, $border , $align, $style);
}
private function AddFooter(){
	$BoxWidth = (($this->GetPageWidth())-20)/4;
	$BoxStartX = 105;
	$BoxYStart = 205;
	$this->setXY($BoxStartX,$BoxYStart);
	$this->SetFont('Arial','B',12);
	$this->Cell($BoxWidth,10,'Subtotal',1,0,'C');
	$this->SetFont('Arial','',12);
	$this->Cell($BoxWidth,10,$this->documentData['subtotal'],1,0,'C');
	$this->ln();
	$this->setXY($BoxStartX,$BoxYStart+10);
	$this->SetFont('Arial','B',12);
	$this->Cell($BoxWidth,10,'Tax',1,0,'C');
	$this->SetFont('Arial','',12);
	$this->Cell($BoxWidth,10,$this->documentData['tax'],1,0,'C');
	$this->setXY($BoxStartX,$BoxYStart+20);
	$this->SetFont('Arial','B',12);
	$this->Cell($BoxWidth,10,'Shipping',1,0,'C');
	$this->SetFont('Arial','',12);
	$this->Cell($BoxWidth,10,$this->documentData['shipping'],1,0,'C');
	$this->ln();
	$this->setXY($BoxStartX,$BoxYStart+30);
	$this->SetFont('Arial','B',12);
	$this->Cell($BoxWidth,10,'Total',1,0,'C');
	$this->SetFont('Arial','',12);
	$this->Cell($BoxWidth,10,$this->documentData['total'],1,0,'C');

	$this->SetAutoPageBreak(false,0);
	$this->SetY(-15);
      // Select Arial italic 8
      $this->SetFont('Arial','I',8);
      // Print centered page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
      $this->SetAutoPageBreak(true,2);
      $this->AliasNbPages();

      $this->SetY(-35);
	$this->SetFont('Arial','I',8);
      $this->Cell(0,10,$this->documentData['footerText'],0,0,'C'); 
} 

//Public 
function setLineItemWidth($widths){
	$total = 0;
	foreach($widths as $width){
		$total += $width; 
	}
	if($total > 190){
		throw new Exception("ERROR:Your total widths are greater than the size of the page. Please reduce the size of your line time headers.", 1);
		
	}
	$this->lineItemWidth = $widths;
}
function setLineItemHeaders($headers){
	$this->lineItemHeaders = $headers;
}

function setCompanyHeader($compHead){
	$this->companyInfo = $compHead;

}
function setDocumentData($data){
	$this->documentData = $data;
}
function setLineItemsData($data){
	$this->lineItems = $data;
}
function createDocument(){
	$this->addPage();
	$this->CreateCompanyHeader();
	$this->CreateBillTo();
	$this->CreateShipTo();
	$this->CreateDetailsRow();
	$this->CreateLineItems();
}

}//End Class
?>