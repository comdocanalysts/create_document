<?php
include_once('PDF_Document.php');
$pdf = new PDF_Document;
$pdf->setCompanyHeader(
	[
		"CompanyName" => "Nathan's Company",
		"CompanyAddress" => "123456 Main Street",
		"CompanyState" => 'OH',
		"CompanyCity" => 'Akron',
		"CompanyZip" => "11234",
		"CompanyPhone" => "330-945-1243",
		"CompanyFax" => "123-123-1234",
		"docType" => "PO",
		"logo" => "logo.png"
	]
);

$pdf->setDocumentData(
	[
		"billTo"=>["Nathan's Company","123456 Main Street","Akron OH, 55432","United States"],
		"shipTo"=> ["Nathan's Company","123456 Main Street","Akron OH, 55432","United States"],
		"Invoice Number"=>"112ANF", 
		"Terms" => '30 Days',
		"PO Number" => '11243',
		"tax"=>"20.45",
		"subtotal"=>"10",
		"shipping"=>"22.36",
		"total"=>"100.00", 
		"footerText" => "Thanks for being a customer at Nathan's Company"
	]
);
$pdf->setLineItemWidth(array(38,38,38,38,38));
$pdf->setLineItemHeaders(array('ItemID','Qty','PriceEa','Desc','Line Total'));
$pdf->setLineItemsData(
	array(
		array("ABC123","10","1.88","IT does a thing", "18.80"),
		array("ABC124","10","1.88","IT does a thing", "18.80"),
		array("ABC125","10","1.88","IT does a thing", "18.80"),
		array("ABC126","10","1.88","IT does a thing", "18.80"),
		array("ABC127","10","1.88","IT does a thing", "18.80"),
		array("ABC128","10","1.88","IT does a thing", "18.80"),
		array("ABC128","10","1.88","IT does a thing", "18.80"),
		array("ABC128","10","1.88","IT does a thing", "18.80"),
		array("ABC128","10","1.88","IT does a thing", "18.80"),
		array("ABC128","10","1.88","IT does a thing", "18.80"),
		array("ABC128","10","1.88","IT does a thing", "18.80"),
		array("ABC129","10","1.88","IT does a thing", "18.80")
	)
);

?>